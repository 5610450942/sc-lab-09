

public class Company implements Taxable{
	private String name;
	private double income;
	private double expense;
	
	public Company(String name,double income,double expense){
		this.name = name;
		this.income = income;
		this.expense = expense;
	}

	
	public double getTax() {
		return (income-expense)*0.3;
	}
	
	public String toString(){
		return name+" income: "+income+" expense: "+expense;
	}


	public double getIncome() {
		// TODO Auto-generated method stub
		return income;
	}


	public double getExpense() {
		// TODO Auto-generated method stub
		return expense;
	}

}
