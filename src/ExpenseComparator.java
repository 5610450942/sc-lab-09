import java.util.Comparator;


public class ExpenseComparator implements Comparator<Company> {

	@Override
	public int compare(Company arg0, Company arg1) {
		// TODO Auto-generated method stub
		if (arg0.getExpense() < arg1.getExpense()){return -1;}
		if (arg0.getExpense() > arg1.getExpense()){return 1;}
		return 0;
	}
}
