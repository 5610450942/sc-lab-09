
public class Person implements Measurable,Taxable,Comparable<Person> {
	
	private String name; 
	private int height;
	private int income;
	public Person(String name,int height,int income){
		this.name = name;
		this.height = height;
		this.income = income;

	}
	public int getIncome(){return income;}
	@Override
	public double getMeasure() {	
		return height;
	}
	
	public String toString(){
		return name+" height: "+height+" income : "+income;
	}
	public double getTax() {
		if(income<=300000)return income*0.05;
		return (300000*0.05)+((income-300000)*0.1);
	}
	@Override
	public int compareTo(Person o) {
		// TODO Auto-generated method stub
		if(income < o.getIncome()){return -1;}
		if(income > o.getIncome()){return 1;}
		return 0;
	}


}
