

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tree.InOrderTraversal;
import tree.Node;
import tree.PostOrderTraversal;
import tree.PreOrderTraversal;
import tree.ReportConsole;


public class Test {

	public static void main(String[] args) {
		setTestCase_1();
		System.out.println("----------------------------------------------------------------");
		setTestCase_2();
		System.out.println("----------------------------------------------------------------");
		setTestCase_3();
		System.out.println("----------------------------------------------------------------");
		setTestCase_4();

	}
	
	private static void setTestCase_1(){
		ArrayList<Person> persons = new ArrayList<Person>();
		persons.add(new Person("Thanawan", 169,3000));
		persons.add(new Person("Nattapol", 167,1000));
		persons.add(new Person("Nattachai", 165,2000));	
		Collections.sort(persons);
		System.out.println("Collects.sort(persons) :");
		for(Person p : persons){
			System.out.println(p);}
	}
	private static void setTestCase_2(){
		ArrayList<Product> products = new ArrayList<Product>();
		products.add(new Product("milk", 10));
		products.add(new Product("chiken",120));
		products.add(new Product("eggs", 48));	
		Collections.sort(products);
		System.out.println("Collects.sort(products) :");
		for(Product p : products){
			System.out.println(p);}
	}
	private static void setTestCase_3(){
		ArrayList<Company> companys = new ArrayList<Company>();
		companys.add(new Company("AIS",  1000000,800000));
		companys.add(new Company("TRUE", 2000000,1000000));
		companys.add(new Company("Happy",1000000,500000));
		Collections.sort(companys,new EarningComparator());
		System.out.println("Collects.sort(companys , EarningComparator) :");
		for(Company p : companys){
			System.out.println(p);}
		Collections.sort(companys,new ExpenseComparator());
		System.out.println("Collects.sort(companys , ExpenseComparator) :");
		for(Company p : companys){
			System.out.println(p);}
		Collections.sort(companys,new ProfitComparator());
		System.out.println("Collects.sort(companys , ProfitComparator) :");
		for(Company p : companys){
			System.out.println(p);}
	}

	public static void setTestCase_4(){
		//person
				ArrayList<Taxable> taxs = new ArrayList<Taxable>();
				taxs.add(new Person("Thanawan",170, 100000));
				taxs.add(new Company("AIS", 1000000,800000));
				taxs.add(new Product("milk", 100));
				Collections.sort(taxs,new TaxComparator());
				System.out.println("Collects.sort(taxs , ProfitComparator) :");
				for(Taxable p : taxs){
					System.out.println(p.getTax());}
			}


}
