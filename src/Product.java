

public class Product implements Taxable,Comparable<Product>{
	private String name;
	private double price;
	public Product(String name,double price){
		this.name = name;
		this.price = price;
	}

	
	public double getTax() {
		return price*0.07;
	}
	
	public String toString(){
		return name+" price: "+price;
	}
	public double getPrice() {
		// TODO Auto-generated method stub
		return price;
	}
	@Override
	public int compareTo(Product o) {
		// TODO Auto-generated method stub
		if(price < o.getPrice()){return -1;}
		if(price > o.getPrice()){return 1;}
		return 0;
}


}
