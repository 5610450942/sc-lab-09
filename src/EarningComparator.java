import java.util.Comparator;


public class EarningComparator implements Comparator<Company> {

	@Override
	public int compare(Company arg0, Company arg1) {
		// TODO Auto-generated method stub
		if (arg0.getIncome() < arg1.getIncome()){return -1;}
		if (arg0.getIncome() > arg1.getIncome()){return 1;}
		return 0;
	}


}
