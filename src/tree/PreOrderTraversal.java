package tree;

import java.util.ArrayList;

public class PreOrderTraversal implements Traversal{
	ArrayList<Node> preOrder = new ArrayList<Node>();
	@Override
	public ArrayList<Node> traverse(Node node) {
		// TODO Auto-generated method stub
		
		Node x = node;
		  if(x == null){ return null;}
		  
		  preOrder.add(x);
		  
		  traverse( x.getLeft() );
		  traverse( x.getRight() ); 
		
		return preOrder;
	}

}
