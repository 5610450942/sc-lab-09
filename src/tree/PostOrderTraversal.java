package tree;

import java.util.ArrayList;

public class PostOrderTraversal implements Traversal{

	ArrayList<Node> postOrder = new ArrayList<Node>();
	@Override
	public ArrayList<Node> traverse(Node node) {
		// TODO Auto-generated method stub
		
		Node x = node;
		  if(x == null){ return null;}
		  
		  
		  traverse( x.getLeft() );
		  traverse( x.getRight() ); 
		  postOrder.add(x);

		
		return postOrder;
	}

}
