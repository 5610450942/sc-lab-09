import java.util.Comparator;


public class TaxComparator implements Comparator<Taxable> {

	@Override
	public int compare(Taxable arg0, Taxable arg1) {
		// TODO Auto-generated method stub
		if (arg0.getTax() < arg1.getTax()){return -1;}
		if (arg0.getTax() > arg1.getTax()){return 1;}
		return 0;
	}
}
